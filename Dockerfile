FROM nginx:alpine

RUN apk --update add python3
RUN pip3 install --upgrade pip

COPY nginx.conf /etc/nginx/nginx.conf
COPY requirements.txt .

RUN pip3 install -r requirements.txt
